import { Product } from "../models/product.model";
import { Observable } from "rxjs";

export interface IProductApplication {
  showProducts(): Observable<Product[]>;
  createProduct(product: Product): Observable<Product>;
  editProduct(product: Product): Observable<Product>;
  showProductId(id: number): Observable<Product>;
  deleteProduct(id: number): Observable<any>;
  productEmpty(): Product;
}
