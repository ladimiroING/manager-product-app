import { MessagesError } from "@core/constants/messages.error";

export interface IErrorMessagesApp {
  showMessagesError(msn: MessagesError): string;
}
