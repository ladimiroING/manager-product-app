import { AbstractControl, ValidationErrors } from "@angular/forms";

export interface FormProduct {
  name: (string | ((control: AbstractControl<any, any>) => ValidationErrors | null))[],
  price: (number | ((control: AbstractControl<any, any>) => ValidationErrors | null))[],
  stock: (number | ((control: AbstractControl<any, any>) => ValidationErrors | null))[]
}
