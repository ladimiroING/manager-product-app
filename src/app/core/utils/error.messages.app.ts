import { Injectable } from "@angular/core";
import { MessagesError } from "@core/constants/messages.error";
import { IErrorMessagesApp } from "@domain/interface/error.messages.app";

@Injectable()
export class ErrorMessagesApp implements IErrorMessagesApp {
  showMessagesError(msn: MessagesError): string {
    return msn;
  }
}
