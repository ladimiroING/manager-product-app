export enum MessagesError {
  NOT_SHOW_PRODUCTS = "Sorry, we have problems with show products",
  NOT_CREATE_PRODUCT = "Sorry, we have problems with create product",
  NOT_EDIT_PRODUCT = "Sorry, we have problems with edit product"
}
