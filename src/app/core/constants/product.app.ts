import { IErrorMessagesApp } from '@domain/interface/error.messages.app';
import { IProductApplication } from '@domain/interface/product.interface.app';

export enum ProductApp {
  IProductApplication,
  IErrorMessagesApp
}
