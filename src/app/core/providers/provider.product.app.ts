import { Provider } from "@angular/core";
import { ProductApp } from "../constants/product.app";
import { ProductApplicationService } from "@services/product.app.service";
import { ErrorMessagesApp } from "@core/utils/error.messages.app";

export const ProviderProductApp: Provider = {
  provide: ProductApp.IProductApplication,
  useClass: ProductApplicationService
}

export const ProviderErrorMessagesApp: Provider = {
  provide: ProductApp.IErrorMessagesApp,
  useClass: ErrorMessagesApp
}


