import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductApp } from 'src/app/core/constants/product.app';
import { IProductApplication } from 'src/app/domain/interface/product.interface.app';
import { Product } from 'src/app/domain/models/product.model';

@Component({
  selector: 'app-show-product-id',
  templateUrl: './show-product-id.component.html',
  styleUrls: ['./show-product-id.component.css']
})
export class ShowProductIdComponent implements OnInit {
  product?: Product;

  constructor(
    @Inject(ProductApp.IProductApplication)
    private _productSVC: IProductApplication,
    private aRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.aRoute.params.subscribe(params => {
      let id: number = params['id'];
      if (id) {
        this._productSVC.showProductId(id).subscribe({
          next: (data: Product) => {
            this.product = data
          },
          error: console.log
        })
      }
    });
  }
}
