import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowProductIdComponent } from './show-product-id.component';

describe('ShowProductIdComponent', () => {
  let component: ShowProductIdComponent;
  let fixture: ComponentFixture<ShowProductIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowProductIdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowProductIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
