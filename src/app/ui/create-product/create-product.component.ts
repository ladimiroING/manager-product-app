import { authValidatorPriceStock, productForm } from './../forms/product.form';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductApp } from '@core/constants/product.app';
import { IProductApplication } from 'src/app/domain/interface/product.interface.app';
import { Product } from 'src/app/domain/models/product.model';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  formProduct!: FormGroup;

  constructor(
    @Inject(ProductApp.IProductApplication)
    private _productSVC: IProductApplication,
    private router: Router,
    private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.InitFormProduct();
  }

  InitFormProduct(): void {
    this.formProduct = this._formBuilder.group(productForm, authValidatorPriceStock);
  }

  listBackProducts() {
    this.router.navigate(['manager-product/show-products']);
  }

  onCreateProduct() {
    if (this.formProduct.valid) {
      this._productSVC.createProduct(this.formProduct.value).subscribe({
        next: (product: Product) => {
          alert('Product added successfully');
          this.listBackProducts();
        },
        error: (err) => {
          console.log(err);
        }
      })
    }
  }
}
