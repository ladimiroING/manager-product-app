import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductComponent } from './create-product/create-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { ShowProductIdComponent } from './show-product-id/show-product-id.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'create-product', component: CreateProductComponent },
      { path: 'edit-product/:id', component: EditProductComponent },
      { path: 'show-products', component: ShowProductsComponent },
      { path: 'show-product/:id', component: ShowProductIdComponent },
      { path: '**', redirectTo: 'show-products' }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentsRoutingModule { }
