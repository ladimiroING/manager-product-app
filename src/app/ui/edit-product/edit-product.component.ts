import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductApp } from 'src/app/core/constants/product.app';
import { IProductApplication } from 'src/app/domain/interface/product.interface.app';
import { Product } from 'src/app/domain/models/product.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  id!: number;
  product!: Product;

  constructor(
    @Inject(ProductApp.IProductApplication)
    private _productSVC: IProductApplication,
    private router: Router,
    private aRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.product = this._productSVC.productEmpty();
    this.id = this.aRoute.snapshot.params['id'];
    this._productSVC.showProductId(this.id).subscribe({
      next: (product: Product) => {
        this.product = product;
      },
      error: console.log
    })
  }

  listBackProducts() {
    this.router.navigate(['manager-product/show-products']);
  }

  onEditProduct() {
    this._productSVC.editProduct(this.product).subscribe({
      next: (product: Product) => {
        console.log(product);
        alert('Update Product Success');
        this.listBackProducts();
      },
      error: console.log
    })
  }
}
