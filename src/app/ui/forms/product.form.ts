import { AbstractControl, AbstractControlOptions, Validators } from "@angular/forms";
import { FormProduct } from "@domain/models/form.product";

export const productForm: FormProduct = {
  name: ['', Validators.required],
  price: [0, Validators.required],
  stock: [0, Validators.required]
}

export const authValidatorPriceStock: AbstractControlOptions = {
  validators:[
    (form: AbstractControl): { [key: string]: any } | null => {
      return (form.get('price')?.value < 0) ? { isPrice: true} : { isPrice: false}
    },
    (form: AbstractControl): {[key: string]: any} | null =>{
      return (form.get('stock')?.value < 0) ? { isStock: true} : { isStock: false}
    }
  ]
}
