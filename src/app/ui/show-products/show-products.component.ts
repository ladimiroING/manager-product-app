import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductApp } from 'src/app/core/constants/product.app';
import { IProductApplication } from 'src/app/domain/interface/product.interface.app';
import { Product } from 'src/app/domain/models/product.model';

@Component({
  selector: 'app-show-products',
  templateUrl: './show-products.component.html',
  styleUrls: ['./show-products.component.css']
})
export class ShowProductsComponent implements OnInit {
  products: Product[] = []

  constructor(
    @Inject(ProductApp.IProductApplication)
    private _productSVC: IProductApplication,
    private router: Router) { }

  ngOnInit(): void {
    this.showProducts();
  }

  showProducts() {
    this._productSVC.showProducts().subscribe({
      next: (response) => {
        console.log(response);
        this.products = response;
      },
      error: (err) => { console.log(err) }
    })
  }

  deleteProduct(id: number) {
    this._productSVC.deleteProduct(id).subscribe({
      next: (data) => {
        alert("product successfully removed");
        window.location.reload();
      }
    })
  }

  listBackProducts() {
    this.router.navigate(['manager-product/show-products']);
  }

  productDetail(id: number) {
    this.router.navigate(['manager-product/show-product/', id]);
  }

  editProduct(id: number) {
    this.router.navigate(['manager-product/edit-product/', id]);
  }
}
