import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

//Components App
import { CreateProductComponent } from './create-product/create-product.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ShowProductIdComponent } from './show-product-id/show-product-id.component';

//Modules App
import { ComponentsRoutingModule } from './components-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Providers
import { ProviderErrorMessagesApp, ProviderProductApp } from '../core/providers/provider.product.app';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CreateProductComponent,
    ShowProductsComponent,
    EditProductComponent,
    ShowProductIdComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule
  ],
  providers:[
    ProviderProductApp,
    ProviderErrorMessagesApp
  ]
})
export class ComponentsModule { }
