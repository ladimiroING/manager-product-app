import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'manager-product',
    loadChildren: () => import('./ui/components.module').then( components => components.ComponentsModule )
  },
  {
    path: '**',
    redirectTo: 'manager-product'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
