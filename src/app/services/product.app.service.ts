import { Inject, Injectable } from "@angular/core";
import { Product } from "@domain/models/product.model";
import { HttpClient } from '@angular/common/http'
import { Observable } from "rxjs";
import { IProductApplication } from "@domain/interface/product.interface.app";
import { ProductApp } from "@core/constants/product.app";
import { IErrorMessagesApp } from "@domain/interface/error.messages.app";
import { MessagesError } from "@core/constants/messages.error";

@Injectable()
export class ProductApplicationService implements IProductApplication {
  private urlProducts: string = 'http://localhost:3000/products';
  private product!: Product;

  constructor(
    @Inject(ProductApp.IErrorMessagesApp)
    private _errorMsn: IErrorMessagesApp,
    private http: HttpClient) {
    this.product = {
      name: "",
      price: 0,
      stock: 0
    }
  }

  productEmpty(): Product {
    return this.product;
  }

  showProducts(): Observable<Product[]> {
    let products = this.http.get<Product[]>(this.urlProducts);
    products.subscribe({
      next: (response) => {
        console.log(response);
      },
      error: () => {
        let message = this._errorMsn.showMessagesError(MessagesError.NOT_SHOW_PRODUCTS);
        throw new Error(message);
      }
    })
    return products;
  }

  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.urlProducts, product);
  }

  editProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.urlProducts + '/' + product.id, product);
  }

  showProductId(id: number): Observable<Product> {
    return this.http.get<Product>(this.urlProducts + '/' + id);
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.urlProducts + '/' + id);
  }
}
